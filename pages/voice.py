import base64
import hashlib
import os
from openai import OpenAI
from flask import jsonify, request
from pathlib import Path
import io
from gradio_client import Client

def elevenApiOutput(args, client):
    if not "voice" in args:
            voice = "Bella"
    else:
        voice = args["voice"]
    text = args["text"]
    result = client.predict(
        text,
        voice,
        fn_index=0
    )
    return (result, open(result, "rb").read())

def openaiApiOutput(args, client: OpenAI):
    if not "voice" in args:
            voice = "onyx"
    else:
        voice = args["voice"]
    text = args["text"]
    result = client.audio.speech.create(
        model=args["voice"],
        voice=voice,
        response_format="mp3",
        input=text
    )
    return result.read()

handlers = {
    "voice-hq-1": elevenApiOutput,
    "voice-hq-2": openaiApiOutput,
    "voice-sq-1": openaiApiOutput
}

def define(gas_app, gas_dbname, data={}):
    #client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))
    client = None
    #gclient = Client("https://elevenlabs-tts.hf.space/")
    gclient = None
    @gas_app.route('/voice/input', methods=['POST'])
    def voice_input():
        """
            NOTE: This function is ALWAYS and ONLY for turning a piece of audio into text. ( STT )
        """
        args = request.get_json()
        if not "voice" in args:
            return jsonify({"status": "error", "message": "No voice provided."})
        else:
            voice = base64.b64decode(args["voice"])
        if not "source" in args:
            return jsonify({"status": "error", "message": "No source provided. Source must be one of: ['grabber', 'voice-hq-1', 'voice-hq-2']"})
        else:
            source = args["source"]
        fname = hashlib.sha256(voice).hexdigest() + ".mp3"
        f = open("tmp/" + fname, "wb")
        f.write(voice)
        f.close()
        resp = data[source].grab("", {
            "openai": client,
            "voice": voice,
            "audio": open("tmp/" + fname, "rb"),
        })
        return jsonify({"status": "success", "response": resp})
    @gas_app.route('/voice/output', methods=['POST'])
    def voice_output():
        """
            NOTE: This function is ALWAYS and ONLY for turning text into audio. ( TTS )
        """
        args = request.get_json()
        if not "text" in args:
            return jsonify({"status": "error", "message": "No text provided."})
        if not "source" in args:
            return jsonify({"status": "error", "message": "No source provided. Source must be one of: ['grabber', 'voice-hq-1', 'voice-hq-2']"})
        if not "voice" in args:
            if source == "voice-hq-1":
                args["voice"] = "Bella"
            elif source == "voice-hq-2":
                args["voice"] = "onyx"
            else:
                source = "voice-sq-2"
                args["voice"] = "onyx"
        source = args["source"]
        if source == "voice-hq-1":
            fname, result = elevenApiOutput({
                "voice": args["voice"],
                "text": args["text"],
            }, gclient)
        if source == "voice-hq-2" or source == "voice-sq-2":
            result = openaiApiOutput({
                "voice": args["voice"],
                "text": args["text"],
                "model": args["model"]
            }, client)
        return jsonify({"status": "success", "response": base64.b64encode(result).decode("utf-8")})