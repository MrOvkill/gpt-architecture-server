import pymongo
import json
import os
import openai
from flask import jsonify, request
from flask_cors import CORS
import uuid
import subprocess
def define(gas_app, gas_dbname, data={}):
    """
    Defines routes for prompt-related API endpoints.

    Parameters:
        gas_app (Flask): The Flask application object.
        gas_dbname (str): The name of the database to connect to.

    Returns:
        None

    Functions:
        prompt_list: Retrieves a list of prompts from the database and returns them as a JSON string.
        prompt_get: Retrieves a prompt from the database based on the provided prompt file.
        prompt_new: Endpoint for creating a new prompt.
    """
    @gas_app.route('/prompts/')
    def prompt_list():
        """
        Retrieves a list of prompts from the database and returns them as a JSON string.

        Returns:
            str: A JSON string containing a list of prompts.
        """
        try:
            data = request.data.json()
        except:
            data = {}
        if not "page" in data:
            data["page"] = 0
        page = data["page"]
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["prompts"]
        prompts = []
        for row in table.find():
            prompts.append({"response": row.get("response"), "prompt": row.get("prompt"), "id": row.get("id")})
        return jsonify(prompts)
    @gas_app.route('/prompts/get/<prompt_id>')
    def prompt_get(prompt_id):
        """
        Retrieves a prompt from the database based on the provided prompt file.

        Parameters:
            prompt_id (str): The prop's UUIDv5

        Returns:
            dict: A dictionary containing the prompt information.
                - id (int): The ID of the prompt.
                - prompt (str): The prompt text.
                - response (str): The response to the prompt.

        Raises:
            KeyError: If the prompt is not found in the database.
        """
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["prompts"]
        prompt = table.find({"id": str(prompt_id)}).limit(1)[0]
        print(prompt)
        if not bool(prompt):
            return json.dumps({"status": "error", "message": "Prompt not found."})
        return jsonify({"id": prompt["id"], "prompt": prompt["prompt"], "response": prompt["response"]})
    @gas_app.route('/prompts/new', methods=['POST'])
    def prompt_new():
        """
        Endpoint for creating a new prompt.
        
        Parameters:
        - None
        
        Returns:
        - JSON response containing the status, ID, prompt, and message of the created prompt.
        """
        data = request.get_json(force=True)
        print("Locating model:")
        if not "model" in data:
            data["model"] = "gpt-3.5-turbo-instruct"
        if not "prompt" in data:
            return json.dumps({"status": "error", "message": "Prompt not found."})
        prompt = data["prompt"]
        print("Found '" + data["model"] + "' model." + "\n Temparature: ")
        if not "temperature" in data:
            data["temperature"] = 0.7
        print(str(data["temperature"]) + "\n Max Tokens: ")
        if not "max_tokens" in data:
            data["max_tokens"] = 1024
        print(str(data["max_tokens"]) + "\n Max Tokens: ")
        if int(data["max_tokens"]) > 2048:
            data["max_tokens"] = 2048
        print(str(data["max_tokens"]) + "\n Max Tokens: ")
        if int(data["max_tokens"]) < len(prompt):
            data["max_tokens"] = len(prompt) + 1
        print(str(data["max_tokens"]) + "\n Top P: ")
        if not "top_p" in data:
            data["top_p"] = 0.95
        print(str(data["top_p"]) + "\n")
        if not "frequency_penalty":
            data["frequency_penalty"] = 0
        if not "presence_penalty":
            data["presence_penalty"] = 0
        print("AKA '" + os.getenv("OPENAI_API_KEY") + "'")
        # completion = openai.Completion.create(
        #     engine=data["model"],
        #     prompt=prompt,
        #     temperature=float(data["temperature"]),
        #     max_tokens=int(data["max_tokens"]),
        #     top_p=float(data["top_p"]),
        #     frequency_penalty=float(data["frequency_penalty"]),
        #     presence_penalty=float(data["presence_penalty"]),
        #     stop=["\n", 'EOS'],
        #     api_key=os.getenv("OPENAI_API_KEY")
        # )
        message = data["grabber"].grab(sender="prompt", message=prompt)
        # use completion.choices[0].text to access value.
        trimmedCompletion = {
            "id": uuid.uuid5(uuid.NAMESPACE_DNS, str(uuid.uuid4())).hex,
            "prompt": prompt,
            "response": completion.choices[0].text,
            "model": data["model"],
            "temperature": data["temperature"],
            "max_tokens": data["max_tokens"],
            "top_p": data["top_p"]
        }
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["prompts"]
        insertion = table.insert_one(trimmedCompletion)
        return jsonify({
            "status": "success",
            "id": trimmedCompletion["id"],
            "prompt": trimmedCompletion["prompt"],
            "message": trimmedCompletion["response"]
        })
    gas_app.add_url_rule('/prompts', 'prompts', prompt_list)
    gas_app.add_url_rule('/prompts/get/<prompt_file>', 'prompt_get', prompt_get)
    gas_app.add_url_rule('/prompts/new', 'prompt_new', prompt_new)