import pymongo
import uuid
from flask import jsonify, request
from samutils import auth, dialogue
from samutils.mod import chat_check
    
def define(gas_app, gas_dbname, data={}):
    @gas_app.route('/chats/')
    def chats_list():
        """
        Retrieves a list of chats from the impbase and returns them as a JSON string.
        """
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["chats"]
        chats = []
        
        for row in table.find():
            chats.append(row)
        return jsonify(chats)
    @gas_app.route('/chats/get/<chat_id>')
    def chat_get(chat_id):
        """
        Retrieves a chat from the impbase based on the provided chat ID.
        """
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["chats"]
        chat = table.find({"id": str(chat_id)}).limit(1)[0]
        return jsonify({
            "id": chat["id"],
            "name": chat["name"],
            "messages": chat["messages"]
        })
    @gas_app.route('/chats/new', methods=['POST'])
    def chat_new():
        """
        Endpoint for creating a new chat.
        """
        imp = request.get_json(force=True)
        if not "name" in imp:
            return jsonify({"status": "error", "message": "No name provided."})
        name = str(imp["name"])
        if not "message" in imp:
            return jsonify({"status": "error", "message": "No message provided."})
        message = imp["message"]
        if not "system" in imp:
            imp["system"] = "You are a helpful assistant."
        if not "model" in imp:
            return jsonify({"status": "error", "message": "No model provided."})
        if not "max_tokens" in imp:
            imp["max_tokens"] = 2048
        if imp["max_tokens"] > 2048:
            imp["max_tokens"] = 2048
        if imp["max_tokens"] < 1:
            imp["max_tokens"] = 1
        if not "temperature" in imp:
            imp["temperature"] = 0.7
        if not "top_p" in imp:
            imp["top_p"] = 0.95
        if not "top_k" in imp:
            imp["top_k"] = 45
        if not "frequency_penalty" in imp:
            imp["frequency_penalty"] = 0
        if not "presence_penalty" in imp:
            imp["presence_penalty"] = 0
        isGPT = False
        if imp["model"] == "gpt-3.5-turbo" or imp["model"] == "gpt-4":
            flagged = chat_check(message, {})
            isGPT = True
        else:
            flagged = {"flagged": False}
        if flagged["flagged"]:
            return jsonify({"status": "error", "message": flagged["message"]})
        chat: dialogue.Chat
        if isGPT:
            chat = dialogue.Chat(system_prompt=imp["system"], chat_format="openai")
        else:
            chat = dialogue.Chat(system_prompt=imp["system"], chat_format="llama")
        chat.name(imp["name"])
        chat.settings(
            model=imp["model"],
            max_tokens=imp["max_tokens"],
            temperature=imp["temperature"],
            top_p=imp["top_p"],
            top_k=imp["top_k"],
            frequency_penalty=imp["frequency_penalty"],
            presence_penalty=imp["presence_penalty"]
        )
        msg = chat.chat(message)
        response: str
        if isGPT:
            response = data["grabber"].grab("user",
                                        msg,
                                        chat.settings()
            )
        else:
            response = data["grabber-2"].grab("user",
                                        msg,
                                        chat.settings()
            )
        chat.assistant(response)
        chat.save()
        return jsonify({
            "status": "success",
            "cid": chat.cid,
            "name": chat.username,
            "messages": chat.messages,
            "settings": chat.settings()
        })
    @gas_app.route('/chats/continue', methods=['POST'])
    def chat_continue():
        """
            Endpoint for continuing an existing chat.
            No need to EVER modify any settings for inference during a conversation.
        """
        imp = request.get_json(force=True)
        print(imp)
        if not "cid" in imp:
            return jsonify({"status": "error", "message": "No cid ( Chat ID ) provided."})
        if not "message" in imp:
            return jsonify({"status": "error", "message": "No message provided."})
        message = imp["message"]
        fnd = dialogue.chat_fromdb(imp["cid"])
        if not fnd["found"]:
            return jsonify({
                "status": "error",
                "message": f"Cannot continue non-existent conversation with cid '{imp['cid']}'!"
            })
        chats: dialogue.Chat
        chats = fnd["value"]
        if chats.chat_format == "openai":
            flagged = chat_check(message, {})
        else:
            flagged = {"flagged": False}
        if flagged["flagged"]:
            return jsonify({"status": "error", "message": flagged["message"]})
        msg: str
        chat = chats.chat(message)
        if chats.chat_format == "openai":
            msg = data["grabber"].grab(chats.username,
                                       chat,
                                       chats.settings()
            )
        else:
            msg = data["grabber-2"].grab(chats.username,
                                         chat,
                                         chats.settings()
            )
        chats.assistant(msg)
        chats.save()
        return jsonify({
            "status": "success",
            "cid": chats.cid,
            "name": chats.username,
            "messages": chats.messages,
            "settings": chats.settings()
        })
    gas_app.add_url_rule('/chats', 'chats_list', chats_list)
    gas_app.add_url_rule('/chats/get/<chat_id>', 'chat_get', chat_get)
    gas_app.add_url_rule('/chats/new', 'chat_new', chat_new)
    gas_app.add_url_rule('/chats/continue', 'chat_continue', chat_continue)