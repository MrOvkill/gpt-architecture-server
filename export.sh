export OPENAI_API_KEY=$(cat ~/.openai/api_key.txt)
export OPENAI_ORGANIZATION_KEY=$(cat ~/.openai/organization_key.txt)
export GAS_DBNAME=$(cat gas_dbname.txt)
export AUTH0_DOMAIN=$(cat ~/.auth0/domain.txt)
