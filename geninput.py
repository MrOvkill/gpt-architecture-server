import samutils.inst as instructions
import samutils.text as text

fields = []

doneAddingFields = False
while not doneAddingFields:
    print("Enter a field name (leave blank to stop adding fields):")
    fieldName = input()
    if fieldName == "":
        doneAddingFields = True
    else:
        print("Enter a field prompt:")
        fieldPrompt = input()
        fields.append({
            "name": fieldName,
            "prompt": fieldPrompt
        })

slug = input("Enter a name for this instruction E.G. timeless_emily_vapeshop: ")

ohno = False

txt = ""

print("Enter the instruction text. Press enter twice to end the instruction. (You can use \\n to add a newline.): ")

while not ohno:
    inp = text.readuntilempty()
    if inp == "":
        txt += "\n"
        if input("\\n?(Y/n)") == "n":
            ohno = True
    else:
        txt += inp + "\n"

instruction = instructions.create(slug, txt, fields)

print("Created instruction with id " + str(instruction["id"]) + ".")