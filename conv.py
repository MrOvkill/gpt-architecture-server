import base64
import hashlib

f = open("mp3.base64", "r")
txt = f.read()
f.close()
f = open("mp3.mp3", "wb")
f.write(base64.b64decode(txt))
f.flush()
f.close()

# f = open("mp3.mp3", "rb")
# txt = f.raw.read()
# f.close()
# print(base64.b64encode(txt))