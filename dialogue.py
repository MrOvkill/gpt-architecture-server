import samutils.rand as rand
import samutils.text as text

def branch(p1, p2, prev, vibes):
    stats = {
        "selfless_options": 0,
        "good_options": 1,
        "neutral_options": 2,
        "bad_options": 1,
        "terrible_options": 0,
        "helpful_options": 0,
    }
    if vibes < 350: 
        stats = {
            "selfless_options": 0,
            "good_options": 1,
            "neutral_options": 0,
            "bad_options": 2,
            "terrible_options": 1,
        }
        if rand.hasonechance(1, (-vibes / 10)):
            stats["helpful_options"] = 1
            stats["bad_options"] = 1
    
    if vibes > 750:
        stats = {
            "selfless_options": 1,
            "good_options": 2,
            "neutral_options": 1,
            "bad_options": 0,
            "terrible_options": 0,
        }
        if rand.hasonechance(1, (vibes/5)):
            stats["helpful_options"] = 1
            stats["good_options"] = 1
    if vibes > 350:
        stats = {
            "selfless_options": 0,
            "good_options": 1,
            "neutral_options": 2,
            "bad_options": 1,
            "terrible_options": 0,
        }

def getinitprompt():
    # Gather input until empty string, replace string '\\' with '\n'.
    ohno = False
    while not ohno:
        inp = text.readuntilempty()