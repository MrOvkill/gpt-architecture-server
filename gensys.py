import samutils.text as text

import json

ln = input("Syschat Slug E.G. my_slug: ")
print("Please enter the prompt: ")
line = text.readuntilempty()

with open("./system/" + ln + ".syschat.json", "w") as f:
    json.dump({"name": ln, "message": line}, f)

print("Done!")