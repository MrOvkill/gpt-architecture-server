from flask import Flask, render_template, request, url_for, flash, redirect, jsonify
from flask_cors import CORS
import os
import openai
import argparse
import logging
from pages import voice
import pages.prompts as prompts
import pages.chat as chats
from samutils import system
import samutils.api as api
import json

parser = argparse.ArgumentParser(description='Run the SAS ( Slimy Architecture Server ) API server.', prog="app.py")
parser.add_argument('--logconsole', '-lc', action='store_true', default=True, help='Log to the console.')
parser.add_argument('--logtext', '-l', type=str, default="log.txt", help='Log to the specified file.')
args = parser.parse_args()

global keys
keys = {
    "OPENAI_API_KEY": os.getenv("OPENAI_API_KEY"),
    "OPENAI_API_ORGANIZATION": os.getenv("OPENAI_API_ORGANIZATION"),
    "GAS_DBNAME": os.getenv("GAS_DBNAME"),
}
openai.organization = "org-drzwAqk9tSfxSpzTvnQmK086"

global gas_dbname
global gas_app
gas_dbname = os.getenv("GAS_DBNAME")

gas_app = Flask(__name__)
CORS(gas_app)

data = {
    "grabber": api.OpenAIGrabber(),
    "voice-hq-1": api.ElevenLabsSpeechGrabber(),
    "voice-hq-2": api.OpenAISpeechGrabber("tts-1-hd"),
    "voice-sq-1": api.OpenAISpeechGrabber("tts-1"),
    "chat-3b-local": api.Local3BGrabber(),
    "img-hq-1": api.OpenAIImageGrabber(),
    "grabber-2": api.TogetherChatGrabber(),
}

"""
URL: /test
"""
def test():
    return open("test.html").read()

if __name__ == '__main__':
    print("Starting server...")
    global logger
    logger = logging.getLogger('GAS')
    logger.setLevel(logging.INFO)
    if args.logconsole:
        logger.addHandler(logging.StreamHandler())
    if args.logtext:
        logger.addHandler(logging.FileHandler(args.logtext))
    logger.info("Starting server...")
    for rule in gas_app.url_map.iter_rules():
        logger.info(rule)
    prompts.define(gas_app, gas_dbname, data)
    chats.define(gas_app, gas_dbname, data)
    system.define(gas_app, gas_dbname, data)
    voice.define(gas_app, gas_dbname, data)
    gas_app.add_url_rule('/test', 'test', test, methods=['GET', 'POST'])
    gas_app.run()
