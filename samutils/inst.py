import os
import json

"""

Section: Instructions

"""

# Make sure the instructions directory exists.
def ensureDirectory():
    if not os.path.exists("./instructions/"):
        os.makedirs("./instructions/")

# Get a list of instruction files.
def getAll():
    ensureDirectory()
    instruction_files = [f for f in os.listdir("./instructions/") if f.endswith('.instruction.json')]
    return instruction_files

# Retrieve an instruction by id.
def retrieve(id):
    ensureDirectory()
    instruction_files = getAll()
    instruction = None
    for instruction_file in instruction_files:
        with open("./instructions/" + instruction_file) as f:
            instruction = json.load(f)
            if instruction["id"] == id:
                break
    return instruction

# Write an instruction to a file.
def write(instruction):
    ensureDirectory()
    instruction_files = getAll()
    instruction_id = len(instruction_files)
    with open("./instructions/" + str(instruction_id) + ".instruction.json", "w") as f:
        json.dump(instruction, f)
    return instruction_id

def create(name, prompt, data):
    instruction = {
        "name": name,
        "prompt": prompt,
        "data": data
    }
    ensureDirectory()
    instruction_files = getAll()
    instruction_id = len(instruction_files)
    instruction["id"] = instruction_id
    with open("./instructions/" + str(instruction_id) + ".instruction.json", "w") as f:
        json.dump(instruction, f)
    return instruction

