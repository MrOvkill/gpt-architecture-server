from uuid import uuid4
import openai
import pymongo
from flask import jsonify, request

def define(gas_app, gas_dbname, data={}):
    @gas_app.route('/system')
    def system_list():
        """
        Retrieves a list of chats from the impbase and returns them as a JSON string.
        """
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["system"]
        chats = []
        for row in table.find():
            chats.append({
                "id": row["id"],
                "chat": row["chat"],
                "description": row["description"],
            })
        return jsonify(chats)
    @gas_app.route('/system/add', methods=['POST'])
    def system_add():
        """
        Endpoint for creating a new chat.
        """
        chat = request.get_json(force=True)
        if not "chat" in chat:
            return jsonify({"status": "error", "message": "No chat provided."})
        
        if not "description" in chat:
            rslt = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=[
                    {
                        "role": "system",
                        "content": "Summarize any given prompt into 3 words or less.",
                    },
                    {
                        "role": "user",
                        "content":  chat["chat"],
                    }
                ]
            )
            description = rslt["choices"][0]["message"]["content"]  
        else:
            description = chat["description"]
        rslt = openai.Moderation.create(input=chat["chat"] + description)
        if rslt["results"][0]["flagged"]:
            return jsonify({"status": "error", "message": "System chat or description flagged as inappropriate."})
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[gas_dbname]
        table = db["system"]
        ins = table.insert_one({
            "id": uuid4(),
            "chat": chat,
            "description": description,
        })
        return jsonify({
            "id": ins["id"],
            "chat": chat,
            "description": description,
        })
    gas_app.add_url_rule('/system', 'system_get', view_func=system_list)
    gas_app.add_url_rule('/system/add', 'system_add', view_func=system_add)