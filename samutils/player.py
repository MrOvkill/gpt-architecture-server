import os
import json

def ensure():
    if not os.path.exists("./players"):
        os.mkdir("./players")

def loadAll():
    ensure()
    files = [f for f in os.listdir("./players") if f.endswith(".player.json")]
    return files

def write(player):
    ensure()
    f = open("./players/" + player["id"] + ".player.json", "w")
    json.dump(player)
    f.flush()
    f.close()


def create(name, details):
    ensure()
    plyayerCount = len(loadAll())
    player = {
        "id": plyayerCount,
        "name": name,
    }
    for key, value in details:
        player[key] = value
    write(player)