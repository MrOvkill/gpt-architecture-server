import samutils.inst as inst

import os
import json

"""

Section: Conversations

"""

def ensureConversationsDir():
    if not os.path.exists("./conversations/"):
        os.makedirs("./conversations/")

def getConversationFiles():
    ensureConversationsDir()
    conversation_files = [f for f in os.listdir("./conversations/") if f.endswith('.conversation.json')]
    return conversation_files

def getConversationMeta():
    ensureConversationsDir()
    conversation_files = getConversationFiles()
    conversation_meta = []
    for conversation_file in conversation_files:
        conversation = getConversation(conversation_file)
        conversation_meta.append({ "id": conversation.id, "name": conversation.name })

def getConversation(conversation_file):
    ensureConversationsDir()
    with open("./conversations/" + str(conversation_file) + ".conversation.json") as f:
        conversation = json.load(f)
    return conversation

def writeConversation(conversation):
    ensureConversationsDir()
    with open("./conversations/" + str(conversation["id"]) + ".conversation.json", "w") as f:
        json.dump(conversation, f)

def addConversation(conversationName, previousChats=[]):
    ensureConversationsDir()
    conversation_files = getConversationFiles()
    conversation_id = len(conversation_files)
    conversation = { "id": conversation_id, "name": conversationName, "messages": previousChats }
    with open("./conversations/" + str(conversation_id) + ".conversation.json", "w") as f:
        json.dump(conversation, f)
    return conversation

"""

Section: System Chats

"""
