import openai

categories = [
    "sexual",
    "hate",
    "harassment",
    "self-harm",
    "sexual/minors",
    "hate/threatening",
    "violence/graphic",
    "self-harm/intent",
    "self-harm/instructions",
    "harassment/threatening",
    "violence",
]

def chat_check(message, data):
    mod = openai.Moderation.create(input=message)
    flagged = mod["results"][0]["flagged"]
    messages = [
        {
            "role": "system",
            "content": "You are a helpful assistant that moderates comments. A person has sent a comment on my service that violates both of our terms of service. Please tell them why comments in these categories are bad.\n",
        },
        {
            "role": "user",
            "content": "These are the categories that have been flagged: ",
        }
    ]
    idx = 0
    for obj in mod["results"][0]["categories"]:
        if obj:
            messages[1]["content"] += categories[idx] + ", "
        idx += 1
            
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages,
        temperature=0.2,
    )
    return {
        "flagged": flagged,
        "message": response["choices"][0]["message"]["content"]
    }