import sqlite3, time

SQLITE_SCHEMA_VERSION = 2

class Prompt():
    def __init__(self, id=-1, prompt="", model="", temperature=0.0, max_tokens=0, top_p=0.0, frequency=0, last_used=0, created=0, modified=0):
        self.id = id
        self.prompt = prompt
        self.model = model
        self.temperature = temperature
        self.max_tokens = max_tokens
        self.top_p = top_p
        self.frequency = frequency
        self.last_used = last_used
        self.created = created
        self.modified = modified

class SDBConnection():
    def __init__(self):
        self.url = "sqlite://./"
        self.port = -1
        self.user = ""
        self.password = ""
        self.db = "gas.db"
        self.conn = None

class SamDB():
    def __init__(self, conn=SDBConnection()):
        self.conn = conn
    def connect(self):
        self.connected = True
        pass
    def dump(self):
        self.initialized = False
        print("DB: Dumping tables for '{0}'".format(self.conn.url + self.conn.db))
        pass
    def init(self):
        self.initialized = True
        print("DB: Initializing tables for '{0}'".format(self.conn.url + self.conn.db))
        pass
    def getPrompts(self):
        pass
    def getPrompt(self, id):
        pass
    def addPrompt(self, prompt):
        pass
    def hasUser(self, googleAccountToken):
        pass

class SQLIteDB(SamDB):
    def __onit___(self):
        super().__init__()
    def dump(self):
        super().dump()
        self.conn.conn.execute("DROP TABLE IF EXISTS meta")
        self.conn.conn.execute("DROP TABLE IF EXISTS prompts")
        self.conn.conn.execute("DROP TABLE IF EXISTS conversations")
        self.conn.conn.execute("DROP TABLE IF EXISTS users")
        self.conn.conn.execute("DROP TABLE IF EXISTS sessions")
        self.conn.conn.execute("DROP TABLE IF EXISTS google_users")
        self.conn.conn.commit()
    def connect(self):
        super().connect()
        self.conn.conn = sqlite3.connect(self.conn.db)
        try:
            version = self.conn.conn.execute("SELECT version FROM meta").fetchall()
            if version[0][0] != SQLITE_SCHEMA_VERSION:
                print("Version mistmatch. ( {0} != {1} )".format(version[0][0], SQLITE_SCHEMA_VERSION))
                self.dump()
                self.init()
        except sqlite3.OperationalError:
            print("No version found.")
            self.dump()
            self.init()
        self.initialized = True
    def init(self):
        if self.initialized:
            self.dump()
        super().init()
        self.conn.conn.execute("""
CREATE TABLE IF NOT EXISTS meta (
    id INTEGER PRIMARY KEY,
    version INTEGER,
    created INTEGER,
    modified INTEGER
)""")
        self.conn.conn.execute("INSERT INTO meta (version, created, modified) VALUES ({0}, {1}, {2})".format(SQLITE_SCHEMA_VERSION, int(time.time()), int(time.time())))
        self.conn.conn.execute("""
CREATE TABLE IF NOT EXISTS prompts (
    id INTEGER PRIMARY KEY, 
    prompt TEXT, 
    model TEXT, 
    temperature REAL, 
    max_tokens INTEGER,
    top_p REAL,
    frequency INTEGER,
    last_used INTEGER,
    created INTEGER,
    modified INTEGER
)
""")
        self.conn.conn.execute("""
CREATE TABLE IF NOT EXISTS conversations (
    id INTEGER PRIMARY KEY, 
    conversation TEXT, 
    model TEXT, 
    temperature REAL, 
    max_tokens INTEGER, 
    top_p REAL, 
    frequency INTEGER, 
    last_used INTEGER, 
    created INTEGER, 
    modified INTEGER
)
""")
        self.conn.conn.execute("""
CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY, 
    username TEXT, 
    password TEXT, 
    email TEXT, 
    created INTEGER, 
    modified INTEGER
)
""")
        self.conn.conn.execute("""
CREATE TABLE IF NOT EXISTS sessions (
    id INTEGER PRIMARY KEY, 
    session TEXT, 
    user INTEGER, 
    created INTEGER, 
    modified INTEGER
)
""")
        self.conn.conn.commit()
    def getPrompts(self):
        rawPrompts = self.conn.conn.execute("SELECT id, prompt FROM prompts")
        prompts = []
        rawPromptData = rawPrompts.fetchall()
        for prompt in rawPromptData:
            prompts.append(Prompt(id=prompt[0], prompt=prompt[1]))
        return prompts
    def getPrompt(self, id):
        rawPrompt = self.conn.conn.execute("SELECT * FROM prompts WHERE id = {0}".format(id))
        if not bool(prompt):
            return prompt
        prompt = rawPrompt.fetchall()
        return Prompt(id=prompt[0][0], prompt=prompt[0][1], model=prompt[0][2], temperature=prompt[0][3], max_tokens=prompt[0][4], top_p=prompt[0][5], frequency=prompt[0][6], last_used=prompt[0][7], created=prompt[0][8], modified=prompt[0][9])