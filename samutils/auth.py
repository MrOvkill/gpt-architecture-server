from functools import wraps
import json
from urllib.request import urlopen
from uuid import uuid4
from flask import jsonify, request
import pymongo
import hashlib
import os

AUTH0_AUDIENCE = os.getenv("AUTH0_AUDIENCE")
if not AUTH0_AUDIENCE:
    AUTH0_AUDIENCE = "https://sas.auth0.com/api/v2/"
AUTH0_DOMAIN = os.getenv("AUTH0_DOMAIN")
AUTH0_CLIENT_ID = os.getenv("AUTH0_CLIENT_ID")
if not AUTH0_CLIENT_ID:
    AUTH0_CLIENT_ID = "sas"

global auth0

global user

auth0 = {
    "audience": AUTH0_AUDIENCE,
    "domain": AUTH0_DOMAIN,
    "client_id": AUTH0_CLIENT_ID,
    "algorithm": "RS256",
}

class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code

class Authit():
    def __init__(self, dbname):
        self.loggedin = False
        self.dbname = dbname
    def hashpass(self, password):
        return hashlib.sha256(password.encode('utf-8')).hexdigest()
    def login(self, email, passwd):
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[self.dbname]
        table = db["users"]
        user = table.find_one({"email": email})
        if not user:
            return {"status": "error", "message": "User not found."}
        if user["password"] != self.hashpass(passwd):
            return {"status": "error", "message": "Password incorrect."}
        self.loggedin = True
        self.id = user["id"]
        self.name = user["name"]
        return {"status": "ok", "message": "Login successful.", "id": user["id"], "name": user["username"]}
    
    def register(self, email, uname, passwd):
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[self.dbname]
        table = db["users"]
        if table.find_one({"username": uname}):
            return {"status": "error", "message": "Username already exists."}
        if table.find_one({"email": email}):
            return {"status": "error", "message": "Email already exists."}
        itm = table.insert_one({
            "id": uuid4(),
            "email": email,
            "username": uname,
            "password": self.hashpass(passwd)
        })
        return {"status": "ok", "message": "User created.", "id": itm["id"]}
        
    def logout(self):
        self.loggedin = False
        self.id = None
        self.name = None
    def currentUser(self):
        return {
            "id": self.id,
            "name": self.name
        }

def yoink_auth_header():
    bearer = request.headers.get("Authorization", None)
    if not bearer:
        raise AuthError({"code": "authorization_header_missing", "description": "Authorization header is expected"}, 401)
    tokn = bearer.split(" ")
    if not tokn or not tokn[0] or not tokn[1] or tokn[0].lower() != "bearer":
        raise AuthError({"code": "invalid_header", "description": "Authorization malformed"}, 401)
    token = tokn[1]
    return token

def define(gas_app, gas_dbname):
    def auth_login():
        try:
            data = request.get_json()
        except: 
            data = {}
        if not "password" in data:
            return jsonify({"status": "error", "message": "No password provided."})
        if not "email" in data:
            return jsonify({"status": "error", "message": "No email provided."})
        if not "username" in data:
            return jsonify({"status": "error", "message": "No username provided."})
        auth = Authit(gas_dbname)
        return auth.login(data["email"], data["password"])
    
    def auth_logout():
        pass
    def auth_register():
        pass
    def auth_getAccount():
        pass
    gas_app.add_url_rule('/auth/login', 'auth_login', auth_login)
    gas_app.add_url_rule('/auth/logout', 'auth_logout', auth_logout)
    gas_app.add_url_rule('/auth/register', 'auth_register', auth_register)
    gas_app.add_url_rule('/auth/getAccount', 'auth_getAccount', auth_getAccount)
