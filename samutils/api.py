import hashlib
import json
import os
import time
import openai
import io
from gradio_client import Client
from openai import OpenAI
import requests

class ChatGrabber():
  """
    Holds any providers for chat message retrieval and generation.
  """
  def __init__(self):
    self.messages = []
  def grab(self, sender, message):
    self.messages.append({
      "role": sender,
      "content": message,
    })

class Local3BGrabber(ChatGrabber):
  def __init__(self):
    super().__init__()
  def grab(self, sender, message, data={}):
    super().grab(sender, message)
    clnt = requests.post("http://localhost:7860/model/chat", headers={
      'Content-Type': "application/json"
    }, data=json.dumps({
      'name': sender,
      'message': message
    }))
    jsn = clnt.json()
    msg = {
      "role": "assistant",
      "content": jsn["response"]["message"]
    }
    self.messages.append(msg)
    return msg

class TextToSpeechGrabber():
  """
    Holds any providers for text-to-speech generation.
    Provides a simple splitting function to turn the grab text into chunks.
  """
  def __init__(self, split_size=255):
    self.chunks = []
    self.audio = []
    self.grabbed = False
    self.split_size = split_size
  def grab(self, text, data={}):
    for i in range(0, len(text), self.split_size):
      self.chunks.append(text[i:i+self.split_size])
    self.grabbed = True

class BalacoonSpeechGrabber(TextToSpeechGrabber):
  def __init__(self):
    super().__init__()
  def grab(self, text, data={}):
    super().grab(text)
    client = Client("https://mrovkill-balacoon-tts.hf.space/api/synthesize")
    if "audio" in data:
      audio = data["audio"]
    else:
      return b""
    f = open(audio, "rb")
    mp3 = io.BytesIO(f.raw.read())
    f.close()
    os.unlink(audio)
    return mp3

class ElevenLabsSpeechGrabber(TextToSpeechGrabber):
  def __init__(self):
    super().__init__()
  def grab(self, text, data={}):
    super().grab(text)
    client = Client("https://elevenlabs-tts.hf.space/")
    voice = "Bella"
    if "voice" in data:
      voice = data["voice"]
    each = lambda chunk, idx, total: print("{} / {}".format(idx, total), hashlib.sha256(chunk.encode()).hexdigest())
    if "each" in data:
      each = data["each"]
    if "audio" in data:
      audio = data["audio"]
    else:
      return b""
    result = client.predict(
      file=audio,
      voice=voice,
      fn_index=0,
    )
    f = open(result, "rb")
    mp3 = io.BytesIO(f.raw.read())
    f.close()
    os.unlink(result)
    return mp3
  
class OpenAISpeechGrabber(TextToSpeechGrabber):
  def __init__(self, model="tts-1"):
    super().__init__()
    self.model = model
  def grab(self, text, data={}):
    super().grab(text)
    text = None
    if "text" in data:
      text = data["text"]
    result = data["openai"].audio.transcriptions.create(model=self.model, input=text)
    pathToAudio = "tmp/" + hashlib.sha256(text.encode()).hexdigest() + ".mp3"
    result.stream_to_file(pathToAudio)
    f = open(pathToAudio, "rb")
    mp3 = f.raw.read()
    f.close()
    os.unlink(pathToAudio)
    return mp3

class OpenAIGrabber(ChatGrabber):
  def __init__(self):
    super().__init__()
  def grab(self, sender, message, data={}):
    super().grab(sender, message)
    result = openai.ChatCompletion.create(
            model=data["model"],
            messages=self.messages,
            max_tokens=data["max_tokens"],
            temperature=data["temperature"],
            top_p=data["top_p"],
            frequency_penalty=data["frequency_penalty"],
            presence_penalty=data["presence_penalty"],
            stop=['\n', 'EOS']
    )
    msg = {
      "role": "assistant",
      "content": result.choices[0]["message"]["content"]
    }
    self.messages.append(msg)
    return msg

class OfflineDefaultTextGrabber(ChatGrabber):
  """
    A completely junk class which exists for when I need to work on the server 
    without internet.
  """
  def __init__(self):
    super().__init__()
  def grab(self, sender, message, data={}):
    super().grab(sender, message)
    msg = {
      "role": "assistant",
      "message": "Some message about stuff."
    }
    return msg

class OpenAIImageGrabber():
  def __init__(self):
    pass
  def grab(self, prompt, data={}):
    oai = OpenAI()
    result = oai.images.generate(
      prompt=prompt,
      n=1,
      size="1024x0124",
      response_format="base64",
      model="dall-e-3",
      quality="standard"
    )
    print(json.dumps(result))
    return result.data[0].b64_json

class TogetherChatGrabber(ChatGrabber):
  def __init__(self):
    super().__init__()
    self.url = "https://api.together.xyz/inference"
  def grab(self, sender, message, data={}):
    super().grab(sender, message)
    print(json.dumps(data))
    if not "model" in data:
      data["model"] = "togethercomputer/CodeLlama-34b-Instruct"
    if not "max_tokens" in data:
      data["max_tokens"] = 2048
    if not "prompt" in data:
      data["prompt"] = ""
    if not "temperature" in data:
      data["temperature"] = 0.33
    if not "top_p" in data:
      data["top_p"] = 0.95
    if not "top_k" in data:
      data["top_k"] = 45
    if not "presence_penalty" in data:
      data["presence_penalty"] = 0
    req = {
        "model": data["model"],
        "max_tokens": data["max_tokens"],
        "prompt": message,
        "request_type": "language-model-inference",
        "temperature": data["temperature"],
        "top_p": data["top_p"],
        "top_k": data["top_k"],
        "repetition_penalty": data["presence_penalty"],
        "stop": [
            "</s>"
        ],
        "negative_prompt": "",
        "sessionKey": "d2b7a9421a4b0eeeeebbc43e537566345fb9f0a8"
    }
    print("Request: \n\n", json.dumps(req), "\n\n")
    res = requests.post(self.url, data=json.dumps(req), json=req, headers={
        "Authorization": "Bearer " + os.getenv("TOGETHER_API_KEY"),
        "Content-Type": "application/json"
    })
    print("Together API got status: ", res.status_code)
    jsn = res.json()
    print("Got output: ", json.dumps(jsn))
    msg = {
      "role": "assistant",
      "content": jsn["output"]["choices"][0]["text"]
    }
    self.messages.append(msg)
    return msg["content"]