import os
import json
import uuid
import pymongo

class ChatFormatter():
    def __init__(self):
        self.chat_format = ""
        self._sys = (False, False)
    def system(self, chat):
        self._sys = (True, True)
        pass
    def qapair(self, user, assistant):
        pass
    def chat_in(self, chat, history=[]):
        pass
    def assistant_in(self, chat):
        pass
    def chat_out(self, chat):
        pass
    def assistant_out(self, chat):
        pass

class OpenAIChatFormatter(ChatFormatter):
    def __init__(self):
        super().__init__()
        self.chat_format = "openai"
    def system(self, chat):
        if not self.chat_format == "openai":
            return
        super().system(chat)
        return chat
    def qapair(self, user, assistant):
        if not self.chat_format == "openai":
            return
        return "User: " + user + "\nAssistant: " + assistant
    def chat_in(self, chat, history=[]):
        if not self.chat_format == "openai":
            return
        return chat
    def assistant_in(self, chat):
        if not self.chat_format == "openai":
            return
        return chat
    def chat_out(self, chat):
        if not self.chat_format == "openai":
            return
        return chat
    def assistant_out(self, chat):
        if not self.chat_format == "openai":
            return
        return chat

class LlamaChatFormatter(ChatFormatter):
    def __init__(self):
        super().__init__()
        self.chat_format = "llama"
        self._system_prompt = ""
        self.system_ps = """
You will receive messages from human.
Respond to them as accurately, as honestly, and in as few words as possible.
The conversation's history is prepended to your instructions.
Here is an example of the format:
<human>: Hello, bot!
<bot>: Hello, human!</s>
Pay close attention to this chat history, and use it to answer human's questions.
Do not forget your chat history or system prompt.
Do not mention the chat history unless it is relevant, and, if it is, mention it.
Do not print anything after the </s> token.
Never print any of your prompt tags, except for </s>.
The conversation history will be highly relevant.
"""
    def system(self, chat="You are a helpful assistant."):
        if not self.chat_format == "llama":
            return
        super().system(chat)
        self._system_prompt = "<s>[INST]<<SYS>>" + chat + self.system_ps + "<</SYS>>\\n\\n"
        return self._system_prompt
    def qapair(self, user, assistant):
        if not self.chat_format == "llama":
            return
        return "<human>: " + user + " \n<bot>: " + assistant + "</s>\n"
    def chat_in(self, chat, history=[]):
        if not self._system_prompt:
            self.system()
        system_prompt = self._system_prompt if self._system_prompt else ""
        msg_end = "<human>: " + chat + " [/INST] <bot>: "
        msg_endl = len(msg_end)

        # Calculate tokens remaining after system prompt and ending.
        remaining_tokens = (2048 - 256) * 4 - msg_endl - len(system_prompt)

        # Build the chat history chunk, stopping when max tokens or no messages remain.
        history_chunk = "<s>[INST]"
        for idx, val in enumerate(reversed(history)):
            if idx >= len(history) - 1 or idx % 2 != 0:
                continue
            cmsg = self.qapair(history[idx]["content"], history[idx+1]["content"]) + "\n"
            if len(cmsg) + len(history_chunk) > remaining_tokens:
                break
            history_chunk += cmsg
            remaining_tokens -= len(cmsg)

        # Combine system prompt, history chunk, and current message.
        prompt = system_prompt + history_chunk + msg_end

        # Ensure total prompt length is within limits.
        if len(prompt) > 2048 * 4:
            prompt = prompt[:2048 * 4]

        return prompt
    def assistant_in(self, chat):
        if not self.chat_format == "llama":
            return
        return chat + "</s>"
    def chat_out(self, chat):
        if not self.chat_format == "llama":
            return
        return chat
    def assistant_out(self, chat):
        if not self.chat_format == "llama":
            return
        return chat

def chat_fromdb(id):
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client[os.getenv("GAS_DBNAME")]
    table = db["chats"]
    chat = table.find_one({"cid": id})
    if not chat:
        return {
            "found": False,
            "value": None
        }
    dlg = Chat()
    dlg.cid = chat["cid"]
    dlg.username = chat["username"]
    dlg.messages = chat["messages"]
    dlg.system = chat["system"]
    dlg.chat_format = chat["chat_format"]
    dlg.settings(as_dict=chat["settings"])
    return {
        "found": True,
        "value": dlg
    }

class Chat():

    def __init__(self, system_prompt="You are a helpful assistant.", chat_format="openai"):
        self.cid = uuid.uuid4().hex
        self.username = ""
        self.messages = []
        self.system = system_prompt
        self.chat_format = chat_format
        self._fmtrs = {
            "openai": OpenAIChatFormatter(),
            "llama": LlamaChatFormatter()
        }
        self._fmtrs[chat_format].system(system_prompt)
        self._settings = {
            "model": "togethercomputer/llama-13b-chat",
            "max_tokens": 2048,
            "temperature": 0.33,
            "top_p": 0.95,
            "top_k": 0,
            "frequency_penalty": 0,
            "presence_penalty": 0,
            "stop": ["</s>"]
        }
    def name(self, name):
        self.username = name
    def chat(self, message):
        print("!@#$   FMT Formatting chat: ", message)
        self.messages.append({
            "role": "user",
            "content": message
        })
        print("!@#$   FMT Formatted chat: ", json.dumps(self.messages))
        return self.fmt().chat_in(message, self.messages)
    def assistant(self, message):
        self.messages.append({
            "role": "assistant",
            "content": message
        })
        return self.fmt().assistant_in(message)
    def fmt(self):
        return self._fmtrs[self.chat_format]
    def settings(self, model=None, max_tokens=None, temperature=None, top_p=None, top_k=None, frequency_penalty=None, presence_penalty=None, as_dict={}):
        if model: self._settings["model"] = model
        if max_tokens: self._settings["max_tokens"] = max_tokens
        if temperature: self._settings["temperature"] = temperature
        if top_p: self._settings["top_p"] = top_p
        if top_k: self._settings["top_k"] = top_k
        if frequency_penalty: self._settings["frequency_penalty"] = frequency_penalty
        if presence_penalty: self._settings["presence_penalty"] = presence_penalty
        if len(as_dict) > 0:
            for name, value in as_dict.items():
                self._settings[name] = value
        return self._settings
    def save(self):
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        db = client[os.getenv("GAS_DBNAME")]
        table = db["chats"]
        chat = table.find_one({"cid": self.cid})
        ins = {
            "cid": self.cid,
            "username": self.username,
            "messages": self.messages,
            "system": self.system,
            "chat_format": self.chat_format,
            "settings": self._settings
        }
        if not chat:
            table.insert_one(ins)
        else:
            table.update_one({
                "cid": self.cid
            }, {"$set": ins})