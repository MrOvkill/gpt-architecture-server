def readuntilempty():
    lines = []
    while True:
        line = input()
        if line == '':
            break
        lines.append(line + '\n')
    return ''.join(lines)
